<?php
/**
 * creation date: 05.01.2019
 *
 * @author          Julian Kern
 * @copyright       Copyright (c) 2007-2019 Julian Kern, twentytwo Solutions (http://www.22-solutions.de)
 * All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

require __DIR__ . '/vendor/autoload.php';

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

$mysqli = new \mysqli(getenv('MYSQL_HOST'), getenv('MYSQL_USER'), getenv('MYSQL_PASSWORD'));
if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}


if ($mysqli->query("DROP DATABASE bar") === TRUE) {
    echo "Database created deleted";
} else {
    echo "Error creating database: " . $mysqli->error;
}


$sql = "CREATE DATABASE bar";
if ($mysqli->query($sql) === TRUE) {
    echo "Database created successfully";
} else {
    echo "Error creating database: " . $mysqli->error;
}

$mysqli->select_db ( "bar");

// sql to create table
$sql = "CREATE TABLE guests (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
firstname VARCHAR(30) NOT NULL,
lastname VARCHAR(30) NOT NULL,
email VARCHAR(50),
reg_date TIMESTAMP
)";

if ($mysqli->query($sql) === TRUE) {
    echo "Table MyGuests created successfully";
} else {
    echo "Error creating table: " . $mysqli->error;
}

$stmt = $mysqli->prepare("INSERT INTO guests set firstname=?, lastname=?, email=?, reg_date=now()");

$firstname = "julian";
$lastname = "kern";
$email = "kern@foo.de";
$stmt->bind_param('sss', $firstname, $lastname, $email);
$stmt->execute();



echo "say hello";