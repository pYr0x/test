<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('first test with docker');
$I->amOnPage('/');
$I->see('say hello');

$I->seeInDatabase('guests', ['firstname' => 'julian', 'lastname' => 'kern']);